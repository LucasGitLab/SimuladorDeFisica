/** file: test-ball.cpp
 ** brief: Tests the bouncing ball simulation
 ** author: Andrea Vedaldi
 **/

#include "ball.h"
#include <iostream>
using namespace std;

int main(int argc, char** argv)
{
  double coordX;
  double coordY;
  
  cout <<"Digite a coordenada X:\n";
  cin >> coordX;

  cout <<"Digite a coordenada Y:\n";
  cin >> coordY;

  Ball ball(coordX, coordY); 

  cout << ball.getCoordX()<< " "<< ball.getCoordY()<<"\n\n\n\n";

  const double dt = 1.0/100 ;

  run(ball, dt);

  /*for (int i = 0 ; i < 1000 ; ++i) {
    ball.step(dt) ;
    ball.display() ;
  }*/

  return 0 ;
}
