# Introdução
    
O projeto consiste no estudo de uma pequena aplicação, como parte prática da disciplina de
Técnica de Programação 1 - TP1, cursada no 2º semestre de 2017, na Universidade da Brasília.
A aplicação executa uma simulação dinâmica de uma bola caindo e saltando sobre o efeito da
gravidade. Durante a execução projeto o funcionamento do programa será investigado em detalhes,
aprendendo-se sobre o funcionamento de todas as classes de objetos, seu uso na construção da 
aplicação, bem como de sua compilação e depuração. Serão explorados importantes nocões de 
programação orientada-a-objetos, tais como herança e interfaces. Também serão abordados os
procedimentos de inclusão e ligação de bibliotecas para geração de saídas gráficas, 
incluindo animações. 
    
Dessa forma, o objetivo do projeto é aprender e reforçar os aspectos da programação 
orientada-a-objetos, por meio da linguagem C++, projetando e implementando classes
na construção de uma aplicação completa.
    
 
# Requisitos de Sistema 
    
Para o execução do projeto serão utilizados a seguinte configuração de requisitos de sistema:
    
Sistema Operacional: Ubuntu 16.04.1

Editor Texto: VI Version 7.4.16.89

GCC Version 5.4.0 20160609
    
    
# Linha de Comando utilizada
    
Para a compilação do programa foi utilizado o arquivo Makefile localizado em:
    
[https://gitlab.com/LucasGitLab/SimuladorDeFisica/blob/3338354ad3a9832073023456379794292f8eccab/Makefile] (https://gitlab.com/LucasGitLab/SimuladorDeFisica/blob/3338354ad3a9832073023456379794292f8eccab/Makefile)
    
Por meio desse arquivo de configuração é possível compilar o arquivo por meio do 
seguinte comando:
    
make test-ball
    
O referido comando gera a seguinte linha de comando:
    
g++  test-ball.cpp ball.cpp ball.h -lGL -LGLU -lglut -o test-ball
    

    
# Descrição dos Arquivos do Programa
    
simulation.h - define a classe Simulation (uma interface);
  
ball.h - define a classe Ball com herança em relação a classe Simulation, 
com seus métodos e atributos;
    
ball.cpp - implementa os métodos da class Ball para manipulação dos 
atributos da bola durante a execução do programa.
    
test-ball - arquivo do programa principal (main).
    
# Saída do Programa
 
A saída do programa consiste nos registros de posição da bola no formato (x, y),
que caracterizam o movimento simulado de uma bola saltando dentro dos limites de
uma caixa.
 
 
# Gráfico - Trajetória da Bola

O gráfico abaixo representa 1000 pontos de posição da bola obtidos obtidos a partir
da saída do programa. Os valores dos registros demostram que a bola respeita os 
limites da caixa, sendo que o ponto central da bola fica sempre a pelo menos 0.1
de distancia das paredes da caixa. Observa-se que o ponto central da bola não toca
a extremidades da parede devido ao raio da bola que é de aproximadamente de 0,1.

![Gráfico](test-ball-grafico.PNG)


 
Inclua o diagrama das classes usadas na seção 3 do roteiro.
Inclua também o gráfico gerado para a Task 4 no seu texto, e explique o funcionamento do programa. Informe qual ferramenta de software ou script foi usado para gerar esse gráfico.
